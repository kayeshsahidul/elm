﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ELM.Models;

namespace ELM.Controllers
{
    [SessionAuth]
    public class DesignationsController : Controller
    {
        private ELMEntities db = new ELMEntities();

        // GET: Designations
        public ActionResult Index()
        {
            return View(db.Designations.ToList());
        }
        

        // GET: Designations/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Designations/Create Post Method
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,DesignationM,DesignationD,UDT,IsActive")] Designation designation)
        {
            if (ModelState.IsValid)
            {
                designation.IsActive = true;
                designation.UDT = System.DateTime.Now;
                db.Designations.Add(designation);
                db.SaveChanges();
                return RedirectToAction("Index");                
            }

            return View(designation);
        }

        // GET: Designations/Edit/5 View
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Designation designation = db.Designations.Find(id);
            if (designation == null)
            {
                return HttpNotFound();
            }
            return View(designation);
        }

        // POST: Designations/Edit/5 Post Method
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,DesignationM,DesignationD,UDT,IsActive")] Designation designation)
        {
            if (ModelState.IsValid)
            {
                designation.UDT = System.DateTime.Now;
                db.Entry(designation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(designation);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
