﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ELM.Models;

namespace ELM.Controllers
{
    [SessionAuth]
    public class LeaveEntitledController : Controller
    {
        private ELMEntities db = new ELMEntities();

        // GET: LeaveEntitled OK
        public ActionResult Index()
        {
            return View(db.LeTitles.ToList());
        }

        public ActionResult Process()
        {
            string query = "EXEC SP_Calc_LeEvery_LeTitle"; //Execute Stored Procedure Command
            db.Database.ExecuteSqlCommand(query); //Execute in Database
            return Json(1, JsonRequestBehavior.AllowGet); //Return Json alwayes Yes
        }
   
        // GET: LeaveEntitled/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LeTitle leTitle = db.LeTitles.Find(id);
            if (leTitle == null)
            {
                return HttpNotFound();
            }
            return View(leTitle);
        }

        // POST: LeaveEntitled/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,LeType,LeEvery,LeMaxQt,LeYear,UDT,IsActive")] LeTitle leTitle)
        {
            if (ModelState.IsValid)
            {
                leTitle.UDT = System.DateTime.Now;
                db.Entry(leTitle).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(leTitle);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
