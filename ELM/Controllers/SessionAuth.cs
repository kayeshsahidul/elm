﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Mvc;

namespace ELM.Controllers
{
    public class SessionAuth : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HttpContext.Current.Session["EmpId"] == null || HttpContext.Current.Session["EmpRole"] == null || HttpContext.Current.Session["EmpIdpk"] == null)
            {
                HttpContext.Current.Session.Clear();
                HttpContext.Current.Session.Abandon();
                filterContext.HttpContext.Response.Redirect("/Home/Login?fw=" + HttpContext.Current.Request.Url);
            }
            base.OnActionExecuting(filterContext);
        }
    }
}