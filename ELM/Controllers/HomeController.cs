﻿using ELM.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ELM.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }
        //post ? access user to app
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserLoginVM loginvm)
        {
            if (ModelState.IsValid)
            {
                bool UserFound = false;

                using (ELMEntities db = new ELMEntities())
                {
                    UserFound = db.Employees.Where(x => x.EmpId.ToLower() == loginvm.EmpId.Trim().ToLower()).ToList().Any(); //validating the user name in p_users table whether the user text is exist or not  
                    if (UserFound)
                    {
                        var Valid_User = db.Employees.Where(a => a.EmpId.Equals(loginvm.EmpId) && a.PwdKey.Equals(loginvm.PwdKey)).FirstOrDefault();
                        if (Valid_User != null)
                        {
                            #region Create Session for Role Id
                            Session["EmpIdpk"] = Valid_User.ID;
                            Session["EmpId"] = Valid_User.EmpId;
                            Session["EmpName"] = Valid_User.EmpName;
                            Session["EmpRole"] = Valid_User.FK_EmpRole;
                            #endregion

                            #region reutrn to url

                            var footPrint = loginvm.footPrint;
                            //var returnUrl = footPrint.Substring(footPrint.IndexOf("?fw=") + 4);
                            //var getAbsolutePath = Request.Url.AbsoluteUri;
                            Uri myUri = new Uri(footPrint);
                            string returnUrl = HttpUtility.ParseQueryString(myUri.Query).Get("fw");

                            if (returnUrl == "")
                            {
                                return RedirectToAction("Index", "Home");
                            }
                            else if (returnUrl == null)
                            {
                                return RedirectToAction("Index", "Home");
                            }
                            else
                            {
                                return Redirect(returnUrl);
                            }
                            #endregion

                        }
                        else
                        {
                            ViewBag.Message = "Please enter the valid credentials!...";
                            return View(loginvm);
                        }
                    }
                    else
                    {
                        ViewBag.Message = "Please enter the valid credentials!...";
                        return View(loginvm);
                    }
                }
            }
            return View(loginvm);
        }
        [AllowAnonymous]
        //get ? clear session
        public ActionResult Logout()
        {
            Session.Abandon();
            Session.Clear();
            return RedirectToAction("Login");
        }
        [SessionAuth]
        //get ? change password view
        public ActionResult ChangePassword()
        {
            if (Session["EmpId"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Logout");
            }
        }
        [SessionAuth]
        //post ? change password
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(UserLoginVM UL)
        {
            if (Session["EmpIdpk"] != null)
            {
                var Id = Session["EmpIdpk"];
                using (ELMEntities db = new ELMEntities())
                {
                    Employee EP = db.Employees.Find(Id);
                    if (EP == null)
                    {
                        return HttpNotFound();
                    }
                    if (ModelState.IsValid)
                    {
                        EP.PwdKey = UL.PwdKey;
                        db.Entry(EP).State = EntityState.Modified;
                        db.SaveChanges();
                        TempData["MSG"] = "OK! Password changed successfully.";
                        return RedirectToAction("Index");
                    }
                    TempData["MSG"] = "Error! While changing password.";
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Logout");
            }
        }
    }
}