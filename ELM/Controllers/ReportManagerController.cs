﻿using ELM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ELM.Controllers
{
    public class ReportManagerController : Controller
    {
        private ELMEntities db = new ELMEntities();
        // GET: ReportManager
        public ActionResult Index()
        {
            ViewBag.Gender = new SelectList(db.Genders, "ID", "GenderType");
            ViewBag.Designation = new SelectList(db.Designations, "ID", "DesignationM");
            ViewBag.Departments = new SelectList(db.Departments, "ID", "DepartmentName");
            ViewBag.Sections = new SelectList(db.Sections, "ID", "SectionName");
            ViewBag.IsAvail = new SelectList(
                new List<SelectListItem>
                {
                                new SelectListItem { Text = "Available", Value ="1"},
                                new SelectListItem { Text = "Not Available", Value ="0"},
                }, "Value", "Text");
            ViewBag.IsActive = new SelectList(
                            new List<SelectListItem>
                            {
                                new SelectListItem { Text = "Active", Value ="1"},
                                new SelectListItem { Text = "Not Active", Value ="0"},
                            }, "Value", "Text");

            ViewBag.LYear = new SelectList(
                new List<SelectListItem>
                {
                                new SelectListItem { Text = "2019", Value ="2019"},
                                new SelectListItem { Text = "2020", Value ="2020"},
                                new SelectListItem { Text = "2021", Value ="2021"},
                                new SelectListItem { Text = "2022", Value ="2022"},
                }, "Value", "Text");
            ViewBag.LType = new SelectList(db.LeTitles, "ID", "LeType");
            return View();
        }
        public ActionResult Employee(RptEmployee rptEmployee)
        {
            IQueryable<VW_Rpt_employees> emp = db.VW_Rpt_employees;
            if (rptEmployee.Gender != null)
            {
                emp = emp.Where(e => e.FK_Gender == rptEmployee.Gender);
            }
            if (rptEmployee.Designation != null)
            {
                emp = emp.Where(e => e.FK_Designation == rptEmployee.Designation);
            }
            if (rptEmployee.Departments != null)
            {
                emp = emp.Where(e => e.ID == rptEmployee.Departments);
            }
            if (rptEmployee.Sections != null)
            {
                emp = emp.Where(e => e.FK_Section == rptEmployee.Sections);
            }
            if (rptEmployee.IsAvail != null)
            {
                int Y = Convert.ToInt32(rptEmployee.IsAvail);
                if (Y == 1)
                {
                    emp = emp.Where(e => e.IsAvail == true);
                }
                else
                {
                    emp = emp.Where(e => e.IsAvail == false);
                }

            }
            if (rptEmployee.IsActive != null)
            {
                int Y = Convert.ToInt32(rptEmployee.IsActive);
                if (Y == 1)
                {
                    emp = emp.Where(e => e.IsActive == true);
                }
                else
                {
                    emp = emp.Where(e => e.IsActive == false);
                }

            }
            return View(emp.ToList());
        }

        public ActionResult LeaveStatus(RptEmployee rptEmployee)
        {
            IQueryable<VW_Rpt_leaveSummary> emp = db.VW_Rpt_leaveSummary;
            if (rptEmployee.Gender != null)
            {
                emp = emp.Where(e => e.FK_Gender == rptEmployee.Gender);
            }
            if (rptEmployee.Designation != null)
            {
                emp = emp.Where(e => e.FK_Designation == rptEmployee.Designation);
            }
            if (rptEmployee.Departments != null)
            {
                emp = emp.Where(e => e.ID == rptEmployee.Departments);
            }
            if (rptEmployee.Sections != null)
            {
                emp = emp.Where(e => e.FK_Section == rptEmployee.Sections);
            }
            if (rptEmployee.LYear != null)
            {
                emp = emp.Where(e => e.LYear == rptEmployee.LYear);
            }
            if (rptEmployee.LType != null)
            {
                emp = emp.Where(e => e.FK_LeType == rptEmployee.LType);
            }
            return View(emp.ToList());
        }
    }
}