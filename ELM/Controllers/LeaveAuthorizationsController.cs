﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ELM.Models;

namespace ELM.Controllers
{
    [SessionAuth]
    public class LeaveAuthorizationsController : Controller
    {
        private ELMEntities db = new ELMEntities();

        // GET: LeaveAuthorizations
        public ActionResult Index()
        {
            return View(db.vw_LeaveAuthorizations.ToList());
        }
        // GET: NewFirstAuth/Create
        public ActionResult NewFirstAuth()
        {
            ViewBag.AuthType = new SelectList(
                            new List<SelectListItem>
                            {
                                new SelectListItem { Text = "1st Approver", Value = 1.ToString()}
                            }, "Value", "Text");
            ViewBag.FK_Employee = new SelectList(db.vwEmpForLevAuths.Where(d => d.ID != 0 && (db.AuthLeaves.Where(x=>x.FK_Employee==d.ID).FirstOrDefault()==null)),"ID","EmpName");            
            ViewBag.FK_Section = new SelectList(db.vwSectionForLevAuths, "ID", "SectionName");
            return View();
        }

        // POST: NewFirstAuth/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewFirstAuth([Bind(Include = "ID,AuthType,IsAuth,FK_Employee,FK_Department,FK_Section")] AuthLeave authLeave)
        {
            if (ModelState.IsValid)
            {
                authLeave.FK_Department = 0;
                authLeave.IsAuth = true;
                db.AuthLeaves.Add(authLeave);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AuthType = new SelectList(
                new List<SelectListItem>
                {
                                new SelectListItem { Text = "1st Approver", Value = 1.ToString()}
                }, "Value", "Text");
            ViewBag.FK_Employee = new SelectList(db.vwEmpForLevAuths.Where(d => d.ID != 0 && (db.AuthLeaves.Where(x => x.FK_Employee == d.ID).FirstOrDefault() == null)), "ID", "EmpName");
            ViewBag.FK_Section = new SelectList(db.vwSectionForLevAuths, "ID", "SectionName");
            return View(authLeave);
        }
        // GET: NewFinalAuth/Create
        public ActionResult NewFinalAuth()
        {
            ViewBag.AuthType = new SelectList(
                new List<SelectListItem>
                {
                                new SelectListItem { Text = "Final Approver", Value = 2.ToString()}
                }, "Value", "Text");
            ViewBag.FK_Employee = new SelectList(db.vwEmpForLevAuths.Where(d => d.ID != 0 && (db.AuthLeaves.Where(x => x.FK_Employee == d.ID).FirstOrDefault() == null)), "ID", "EmpName");
            ViewBag.FK_Department = new SelectList(db.Departments, "ID", "DepartmentName");
            return View();
        }

        // POST: NewFinalAuth/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewFinalAuth([Bind(Include = "ID,AuthType,IsAuth,FK_Employee,FK_Department,FK_Section")] AuthLeave authLeave)
        {
            if (ModelState.IsValid)
            {
                authLeave.FK_Section = 0;
                authLeave.IsAuth = true;
                db.AuthLeaves.Add(authLeave);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AuthType = new SelectList(
                new List<SelectListItem>
                {
                                new SelectListItem { Text = "Final Approver", Value = 2.ToString()}
                }, "Value", "Text");
            ViewBag.FK_Employee = new SelectList(db.vwEmpForLevAuths.Where(d => d.ID != 0 && (db.AuthLeaves.Where(x => x.FK_Employee == d.ID).FirstOrDefault() == null)), "ID", "EmpName");
            ViewBag.FK_Department = new SelectList(db.Departments, "ID", "DepartmentName");
            return View(authLeave);
        }

        // GET: LeaveAuthorizations/Edit/5
        public ActionResult ChangeToFinal(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AuthLeave authLeave = db.AuthLeaves.Find(id);
            if (authLeave == null)
            {
                return HttpNotFound();
            }
            ViewBag.AuthType = new SelectList(
                                new List<SelectListItem>
                                {
                             new SelectListItem { Text = "Final Approver", Value = 2.ToString()}
                                }, "Value", "Text");
            ViewBag.FK_Department = new SelectList(db.Departments, "ID", "DepartmentName");
            return View(authLeave);
        }

        // POST: LeaveAuthorizations/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeToFinal([Bind(Include = "ID,AuthType,IsAuth,FK_Employee,FK_Department,FK_Section")] AuthLeave authLeave)
        {
            if (ModelState.IsValid)
            {
                authLeave.FK_Section = 0;
                authLeave.IsAuth = true;
                db.Entry(authLeave).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AuthType = new SelectList(
                    new List<SelectListItem>
                    {
                             new SelectListItem { Text = "Final Approver", Value = 2.ToString()}
                    }, "Value", "Text");
            ViewBag.FK_Department = new SelectList(db.Departments, "ID", "DepartmentName");
            return View(authLeave);
        }

        // GET: LeaveAuthorizations/Edit/5
        public ActionResult ChangeToFirst(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AuthLeave authLeave = db.AuthLeaves.Find(id);
            if (authLeave == null)
            {
                return HttpNotFound();
            }
            ViewBag.AuthType = new SelectList(
                    new List<SelectListItem>
                    {
                                new SelectListItem { Text = "1st Approver", Value = 1.ToString()}
                    }, "Value", "Text");
            ViewBag.FK_Section = new SelectList(db.vwSectionForLevAuths, "ID", "SectionName");
            return View(authLeave);
        }

        // POST: LeaveAuthorizations/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeToFirst([Bind(Include = "ID,AuthType,IsAuth,FK_Employee,FK_Department,FK_Section")] AuthLeave authLeave)
        {
            if (ModelState.IsValid)
            {
                authLeave.FK_Department = 0;
                authLeave.IsAuth = true;
                db.Entry(authLeave).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AuthType = new SelectList(
                    new List<SelectListItem>
                    {
                                new SelectListItem { Text = "1st Approver", Value = 1.ToString()}
                    }, "Value", "Text");
            ViewBag.FK_Section = new SelectList(db.vwSectionForLevAuths, "ID", "SectionName");
            return View(authLeave);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
