﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ELM.Models;

namespace ELM.Controllers
{
    [SessionAuth]
    public class LeaveApplicationController : Controller
    {
        private ELMEntities db = new ELMEntities();

        // GET: LeaveApplication
        public ActionResult Index()
        {
            if (Session["EmpIdpk"] != null)
            {
                int EmpID = Convert.ToInt32(Session["EmpIdpk"]);
                var LBalance = db.LeAppsSums.Where(l => l.FK_Employee.Equals(EmpID)).ToList();
                if (LBalance != null || LBalance.Count() != 0)
                {
                    var SSL = LBalance.Where(s => s.FK_LeTitle.Equals(1)).ToList();
                    if (SSL.Count() != 0)
                    {
                        ViewBag.SL = SSL.FirstOrDefault().LeBalance;
                        ViewBag.SL1 = SSL.FirstOrDefault().LeUsed;
                    }
                    var CCL = LBalance.Where(s => s.FK_LeTitle.Equals(3)).ToList();
                    if (CCL.Count() != 0)
                    {
                        ViewBag.CL = CCL.FirstOrDefault().LeBalance;
                        ViewBag.CL1 = CCL.FirstOrDefault().LeUsed;
                    }
                    var EEL = LBalance.Where(s => s.FK_LeTitle.Equals(2)).ToList();
                    if (EEL.Count() != 0)
                    {
                        ViewBag.EL = EEL.FirstOrDefault().LeBalance;
                        ViewBag.EL1 = EEL.FirstOrDefault().LeUsed;
                    }
                }
                return View(db.LeApps.Where(a => a.FK_Employee == EmpID).ToList());
            }
            else
            {
                return RedirectToAction("Logout", "Home");
            }
        }

        // GET: LeaveApplication/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LeApp leApp = db.LeApps.Find(id);
            if (leApp == null)
            {
                return HttpNotFound();
            }
            return View(leApp);
        }

        // GET: LeaveApplication/Create
        public ActionResult Create()
        {
            if (Session["EmpIdpk"] != null)
            {
                int EmpID = Convert.ToInt32(Session["EmpIdpk"]);
                ViewBag.FK_Employee = new SelectList(db.Employees.Where(e => e.ID == EmpID), "ID", "EmpName");
                ViewBag.FK_LeTitle = new SelectList(db.LeTitles, "ID", "LeType");
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "Home");
            }

        }
        // POST: LeaveApplication/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,FromDates,ToDates,LYear,LeQty,LeaveReason,WreStayInLeave,LeDocuments,DocName,DocNameNew,IsHalfLe,FK_Employee,FK_LeTitle,FK_ApproverA,ApproverA,FK_ApproverB,ApproverB,IsRejected,UDT")] LeApp leApp)
        {
            int EmpID = Convert.ToInt32(Session["EmpIdpk"]);

            var LQty = (leApp.ToDates - leApp.FromDates).TotalDays;
            if (LQty == 0) //if set single date make it 1 day
            {
                LQty = 1;
            }
            if (LQty < 0)
            {
                ViewBag.Err = "Sorry!. Negetive days not applied";
                ViewBag.FK_Employee = new SelectList(db.Employees.Where(e => e.ID == EmpID), "ID", "EmpName");
                ViewBag.FK_LeTitle = new SelectList(db.LeTitles, "ID", "LeType");
                return View(leApp);
            }
            LQty = LQty + 1;
            bool IsHalf = false;
            if (leApp.IsHalfLe == null)
            {
                IsHalf = false;
            }
            else if (leApp.IsHalfLe == false)
            {
                IsHalf = false;
            }
            else
            {
                IsHalf = true;
                if (LQty == 1)
                {
                    LQty = 0.5; //if today half leave
                }
                else
                {
                    LQty = LQty - 0.5; //if today half but past days are full leave
                }
            }
            //caculate before apply leave
            var LBalance = db.LeAppsSums.Where(l => l.FK_Employee.Equals(leApp.FK_Employee) && l.FK_LeTitle.Equals(leApp.FK_LeTitle)).ToList();
            if (LBalance.Count() != 0)
            {
                var Balance = LBalance.FirstOrDefault().LeBalance;
                var Used = LBalance.FirstOrDefault().LeUsed;
                var Sum = Balance - Used;
                if (Convert.ToDecimal(LQty) > Sum)
                {
                    ViewBag.Err = "Sorry!. You have not enough balance.";
                    ViewBag.FK_Employee = new SelectList(db.Employees.Where(e => e.ID == EmpID), "ID", "EmpName");
                    ViewBag.FK_LeTitle = new SelectList(db.LeTitles, "ID", "LeType");
                    return View(leApp);
                }
            }
            else
            {
                ViewBag.FK_Employee = new SelectList(db.Employees.Where(e => e.ID == EmpID), "ID", "EmpName");
                ViewBag.FK_LeTitle = new SelectList(db.LeTitles, "ID", "LeType");
                ViewBag.Err = "Sorry!. You have not enough balance.";
                return View(leApp);
            }
            if (ModelState.IsValid)
            {
                leApp.LeQty = Convert.ToDecimal(LQty);
                leApp.LYear = System.DateTime.Now.Year;
                leApp.UDT = System.DateTime.Now;
                leApp.IsHalfLe = IsHalf;
                db.LeApps.Add(leApp);
                db.SaveChanges();
                //Execute Procedure
                SP_Single_Leave_Plus_LeAppsSum(leApp.LeQty, leApp.LYear, leApp.FK_Employee, leApp.FK_LeTitle);
                return RedirectToAction("Index");
            }

            ViewBag.FK_Employee = new SelectList(db.Employees.Where(e => e.ID == EmpID), "ID", "EmpName");
            ViewBag.FK_LeTitle = new SelectList(db.LeTitles, "ID", "LeType");
            return View(leApp);
        }

        // GET: LeaveApplication/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LeApp leApp = db.LeApps.Find(id);
            if (leApp == null)
            {
                return HttpNotFound();
            }
            return View(leApp);
        }

        // POST: LeaveApplication/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,FromDates,ToDates,LYear,LeQty,LeaveReason,WreStayInLeave,LeDocuments,DocName,DocNameNew,IsHalfLe,FK_Employee,FK_LeTitle,FK_ApproverA,ApproverA,FK_ApproverB,ApproverB,IsRejected,UDT")] LeApp leApp)
        {
            if (ModelState.IsValid)
            {
                db.Entry(leApp).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(leApp);
        }

        //Delete L App
        public ActionResult Revoke(long id)
        {
            LeApp leApp = db.LeApps.Find(id);
            db.LeApps.Remove(leApp);
            db.SaveChanges();
            //Execute Procedure
            SP_Single_Leave_Minus_LeAppsSum(leApp.LeQty, leApp.LYear, leApp.FK_Employee, leApp.FK_LeTitle);
            return RedirectToAction("Index");
        }
        //Multiple L App [GET]
        public ActionResult MoreLeaveApply()
        {
            if (Session["EmpIdpk"] != null)
            {
                int EmpID = Convert.ToInt32(Session["EmpIdpk"]);
                ViewBag.FK_Employee = new SelectList(db.Employees.Where(e => e.ID == EmpID), "ID", "EmpName");
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "Home");
            }
        }
        //Multiple L App [POST]
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult MoreLeaveApply([Bind(Include = "CLFK_LeTitle,CLfrom,CLto,SLFK_LeTitle,SLfrom,SLto,ELFK_LeTitle,ELform,ELto,LeaveReason,WreStayInLeave,LeDocuments,IsHalfLe,FK_Employee,")] MoreLeaveApplyVM leApp)
        public ActionResult MoreLeaveApply(MoreLeaveApplyVM leApp)
        {
            int EmpID = Convert.ToInt32(Session["EmpIdpk"]);
            #region HalfLe
            //bool IsHalf = false;
            //if (leApp.IsHalfLe == null)
            //{
            //    IsHalf = false;
            //}
            //else if (leApp.IsHalfLe == false)
            //{
            //    IsHalf = false;
            //}
            //else
            //{
            //    IsHalf = true;
            //    if (LQty == 1)
            //    {
            //        LQty = 0.5; //if today half leave
            //    }
            //    else
            //    {
            //        LQty = LQty - 0.5; //if today half but past days are full leave
            //    }
            //}
            #endregion  
            #region CL

            if (leApp.CLFK_LeTitle != false)
            {
                var CL = (leApp.CLto - leApp.CLfrom).TotalDays + 1;
                if (CL == 0) //if set single date make it 1 day
                {
                    CL = 1;
                }
                //caculate before apply leave
                var CLBalance = db.LeAppsSums.Where(l => l.FK_Employee.Equals(leApp.FK_Employee) && l.FK_LeTitle.Equals(3)).ToList();
                if (CLBalance.Count() != 0)
                {
                    var Balance = CLBalance.FirstOrDefault().LeBalance;
                    var Used = CLBalance.FirstOrDefault().LeUsed;
                    var Sum = Balance - Used;
                    if (Convert.ToDecimal(CL) > Sum)
                    {
                        ViewBag.Err = "Sorry!. [CL] You have not enough balance.";
                        ViewBag.FK_Employee = new SelectList(db.Employees.Where(e => e.ID == EmpID), "ID", "EmpName");
                        return View(leApp);
                    }
                }
                else
                {
                    ViewBag.FK_Employee = new SelectList(db.Employees.Where(e => e.ID == EmpID), "ID", "EmpName");
                    ViewBag.Err = "Sorry!. [CL] You have not enough balance.";
                    return View(leApp);
                }
                //Make Save it
                if (ModelState.IsValid)
                {
                    LeApp LA = new LeApp();
                    LA.FromDates = leApp.CLfrom;
                    LA.ToDates = leApp.CLto;
                    LA.LYear = System.DateTime.Now.Year; ;
                    LA.LeQty = Convert.ToDecimal(CL);
                    LA.LeaveReason = leApp.LeaveReason;
                    LA.WreStayInLeave = leApp.WreStayInLeave;
                    LA.LeDocuments = leApp.LeDocuments;
                    LA.FK_Employee = EmpID;
                    LA.FK_LeTitle = 3;
                    LA.UDT = System.DateTime.Now;
                    LA.IsHalfLe = false;
                    db.LeApps.Add(LA);
                    db.SaveChanges();
                    //Execute Procedure
                    SP_Single_Leave_Plus_LeAppsSum(LA.LeQty, LA.LYear, LA.FK_Employee, LA.FK_LeTitle);
                    ViewBag.CLmsg = "Only CL Posted!";
                }

            }
            #endregion
            #region SL

            if (leApp.SLFK_LeTitle != false)
            {
                var SL = (leApp.SLto - leApp.SLfrom).TotalDays + 1;
                if (SL == 0) //if set single date make it 1 day
                {
                    SL = 1;
                }
                //caculate before apply leave
                var SLBalance = db.LeAppsSums.Where(l => l.FK_Employee.Equals(leApp.FK_Employee) && l.FK_LeTitle.Equals(1)).ToList();
                if (SLBalance.Count() != 0)
                {
                    var Balance = SLBalance.FirstOrDefault().LeBalance;
                    var Used = SLBalance.FirstOrDefault().LeUsed;
                    var Sum = Balance - Used;
                    if (Convert.ToDecimal(SL) > Sum)
                    {
                        ViewBag.Err = "Sorry!. [SL] You have not enough balance.";
                        ViewBag.FK_Employee = new SelectList(db.Employees.Where(e => e.ID == EmpID), "ID", "EmpName");
                        return View(leApp);
                    }
                }
                else
                {
                    ViewBag.FK_Employee = new SelectList(db.Employees.Where(e => e.ID == EmpID), "ID", "EmpName");
                    ViewBag.Err = "Sorry!. [SL] You have not enough balance.";
                    return View(leApp);
                }
                //Make Save it
                if (ModelState.IsValid)
                {
                    LeApp LA = new LeApp();
                    LA.FromDates = leApp.SLfrom;
                    LA.ToDates = leApp.SLto;
                    LA.LYear = System.DateTime.Now.Year; ;
                    LA.LeQty = Convert.ToDecimal(SL);
                    LA.LeaveReason = leApp.LeaveReason;
                    LA.WreStayInLeave = leApp.WreStayInLeave;
                    LA.LeDocuments = leApp.LeDocuments;
                    LA.FK_Employee = EmpID;
                    LA.FK_LeTitle = 1;
                    LA.UDT = System.DateTime.Now;
                    LA.IsHalfLe = false;
                    db.LeApps.Add(LA);
                    db.SaveChanges();
                    //Execute Procedure
                    SP_Single_Leave_Plus_LeAppsSum(LA.LeQty, LA.LYear, LA.FK_Employee, LA.FK_LeTitle);
                    ViewBag.SLmsg = "Only SL Posted!";
                }
            }
            #endregion
            #region EL
            if (leApp.ELFK_LeTitle != false)
            {
                var EL = (leApp.ELto - leApp.ELform).TotalDays + 1;
                if (EL == 0) //if set single date make it 1 day
                {
                    EL = 1;
                }
                //caculate before apply leave
                var ELBalance = db.LeAppsSums.Where(l => l.FK_Employee.Equals(leApp.FK_Employee) && l.FK_LeTitle.Equals(2)).ToList();
                if (ELBalance.Count() != 0)
                {
                    var Balance = ELBalance.FirstOrDefault().LeBalance;
                    var Used = ELBalance.FirstOrDefault().LeUsed;
                    var Sum = Balance - Used;
                    if (Convert.ToDecimal(EL) > Sum)
                    {
                        ViewBag.Err = "Sorry!. [EL] You have not enough balance.";
                        ViewBag.FK_Employee = new SelectList(db.Employees.Where(e => e.ID == EmpID), "ID", "EmpName");
                        return View(leApp);
                    }
                }
                else
                {
                    ViewBag.FK_Employee = new SelectList(db.Employees.Where(e => e.ID == EmpID), "ID", "EmpName");
                    ViewBag.Err = "Sorry!. [EL] You have not enough balance.";
                    return View(leApp);
                }
                //Make Save it
                if (ModelState.IsValid)
                {
                    LeApp LA = new LeApp();
                    LA.FromDates = leApp.ELform;
                    LA.ToDates = leApp.ELto;
                    LA.LYear = System.DateTime.Now.Year; ;
                    LA.LeQty = Convert.ToDecimal(EL);
                    LA.LeaveReason = leApp.LeaveReason;
                    LA.WreStayInLeave = leApp.WreStayInLeave;
                    LA.LeDocuments = leApp.LeDocuments;
                    LA.FK_Employee = EmpID;
                    LA.FK_LeTitle = 2;
                    LA.UDT = System.DateTime.Now;
                    //LA.IsHalfLe = IsHalf;
                    db.LeApps.Add(LA);
                    db.SaveChanges();
                    //Execute Procedure
                    SP_Single_Leave_Plus_LeAppsSum(LA.LeQty, LA.LYear, LA.FK_Employee, LA.FK_LeTitle);
                    ViewBag.ELmsg = "Only EL Posted!";
                }
            }
            #endregion

            return RedirectToAction("Index");
        }

        public ActionResult CheckBalance(int Ltype, DateTime d1, DateTime d2, int usr)
        {
            var EL = (d2 - d1).TotalDays; 
            if (EL == 0) //if set single date make it 1 day
            {
                EL = 1;
            }
            if (EL < 0)
            {
                return Json(2);
            }
            EL = EL + 1;
            //caculate before apply leave
            var ELBalance = db.LeAppsSums.Where(l => l.FK_Employee.Equals(usr) && l.FK_LeTitle.Equals(Ltype)).ToList();
            if (ELBalance.Count() != 0)
            {
                var Balance = ELBalance.FirstOrDefault().LeBalance;
                var Used = ELBalance.FirstOrDefault().LeUsed;
                var Sum = Balance - Used;
                if (Convert.ToDecimal(EL) > Sum)
                {
                    return Json(1);
                }
                else
                {
                    return Json(0);
                }
            }
            return null;
        }
        
        //Concurrent Balance (+)
        public void SP_Single_Leave_Plus_LeAppsSum(decimal LQty, int LYear, int Emp, int Ltype)
        {
            string query = "EXEC  SP_Single_Leave_Plus_LeAppsSum @LQty='" + LQty + "', @LYear='" + LYear + "',@Emp='" + Emp + "', @Ltype='" + Ltype + "'";            //Execute Stored Procedure Command
            db.Database.ExecuteSqlCommand(query); //Execute in Database
           // return Json(1, JsonRequestBehavior.AllowGet); //Return Json alwayes Yes
        }
        //Concurrent Balance (-)
        public void SP_Single_Leave_Minus_LeAppsSum (decimal LQty,int LYear, int Emp, int Ltype)
        {
            string query = "EXEC  SP_Single_Leave_Minus_LeAppsSum @LQty='" + LQty + "', @LYear='" + LYear + "',@Emp='"+ Emp + "', @Ltype='"+Ltype+"'";            //Execute Stored Procedure Command
            db.Database.ExecuteSqlCommand(query); //Execute in Database
         //   return Json(1, JsonRequestBehavior.AllowGet); //Return Json alwayes Yes
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
