﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ELM.Models;

namespace ELM.Controllers
{
    [SessionAuth]
    public class SectionsController : Controller
    {
        private ELMEntities db = new ELMEntities();

        // GET: Sections
        public ActionResult Index()
        {
            return View(db.vw_Sections.ToList());
        }

     
        // GET: Sections/Create
        public ActionResult Create()
        {
            ViewBag.FK_Department = new SelectList(db.Departments.Where(d => d.IsActive == true), "ID", "DepartmentName");
            return View();
        }

        // POST: Sections/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,SectionName,FK_Department,IsActive,UDT")] Section  Sections)
        {
            if (ModelState.IsValid)
            {
                Sections.IsActive = true;
                Sections.UDT = System.DateTime.Now;
                db.Sections.Add(Sections);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FK_Department = new SelectList(db.Departments.Where(d => d.IsActive == true), "ID", "DepartmentName");
            return View(Sections);
        }

        // GET: Sections/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Section vw_Sections = db.Sections.Find(id);
            if (vw_Sections == null)
            {
                return HttpNotFound();
            }
            ViewBag.FK_Department = new SelectList(db.Departments.Where(d => d.IsActive == true), "ID", "DepartmentName");
            return View(vw_Sections);
        }

        // POST: Sections/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,SectionName,FK_Department,IsActive,UDT")] Section Sections)
        {
            if (ModelState.IsValid)
            {
                Sections.UDT = System.DateTime.Now;
                db.Entry(Sections).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FK_Department = new SelectList(db.Departments.Where(d => d.IsActive == true), "ID", "DepartmentName");
            return View(Sections);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
