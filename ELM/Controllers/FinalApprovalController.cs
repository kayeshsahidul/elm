﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ELM.Models;

namespace ELM.Controllers
{
    [SessionAuth]
    public class FinalApprovalController : Controller
    {
        private ELMEntities db = new ELMEntities();

        // GET: FinalApproval
        public ActionResult Index()
        {
            if (Session["EmpIdpk"] != null)
            {
                string EmpID = Session["EmpId"].ToString();
                return View(db.vwLeaveSecondAuthorizes.Where(x => x.FinalAuthorize == EmpID).OrderBy(a=>a.EmpId).ToList());
            }
            else
            {
                return RedirectToAction("Logout", "Home");
            }
        }
        public ActionResult Approve(int? id)
        {
            if (Session["EmpIdpk"] != null)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                LeApp LeApp = db.LeApps.Find(id);
                if (LeApp == null)
                {
                    return HttpNotFound();
                }
                if (ModelState.IsValid)
                {
                    LeApp.UDT = System.DateTime.Now;
                    LeApp.FK_ApproverB = Convert.ToInt32(Session["EmpIdpk"]);
                    db.Entry(LeApp).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "Home");
            }
        }

        public ActionResult Reject(int? id)
        {

            if (Session["EmpIdpk"] != null)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                LeApp LeApp = db.LeApps.Find(id);
                if (LeApp == null)
                {
                    return HttpNotFound();
                }
                if (ModelState.IsValid)
                {
                    LeApp.UDT = System.DateTime.Now;
                    LeApp.FK_ApproverB = Convert.ToInt32(Session["EmpIdpk"]);
                    LeApp.IsRejected = true;
                    db.Entry(LeApp).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "Home");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
