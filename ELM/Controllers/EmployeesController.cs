﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ELM.Models;

namespace ELM.Controllers
{
    [SessionAuth]
    public class EmployeesController : Controller
    {
        private ELMEntities db = new ELMEntities();

        // GET: Employees
        public ActionResult Index()
        {
            return View(db.vwEmployees.ToList());
        }

        public ActionResult LeaveProccess()
        {
            return View();
        }

        public ActionResult IsAvailForLeaveEmployee()
        {
            string query = "EXEC SP_IsAvailForLeave_Employee"; //Execute Stored Procedure Command
            db.Database.ExecuteSqlCommand(query); //Execute in Database
            return Json(1, JsonRequestBehavior.AllowGet); //Return Json alwayes Yes
        }

        public ActionResult InsertFromEmployee(string Year, string LT)
        {
            string query = "EXEC SP_InsertFrom_Employee @StartYear='" + Year + "', @LeTypes='" + LT + "'"; //Execute Stored Procedure Command
            db.Database.ExecuteSqlCommand(query); //Execute in Database
            return Json(1, JsonRequestBehavior.AllowGet); //Return Json alwayes Yes
        }
        
       public ActionResult CalcLeaveBalanceLeAppsSum(string Year)
        {
            string query = "SP_Calc_Leave_Balance_LeAppsSum @StartYear='"+Year+"'"; //Execute Stored Procedure Command
            db.Database.ExecuteSqlCommand(query); //Execute in Database
            return Json(1, JsonRequestBehavior.AllowGet); //Return Json alwayes Yes
        }
        // GET: Employees/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // GET: Employees/Create
        public ActionResult Create()
        {
            ViewBag.FK_Gender = new SelectList(db.Genders, "ID", "GenderType");
            ViewBag.FK_Designation = new SelectList(db.Designations, "ID", "DesignationM");
            ViewBag.FK_Section = new SelectList(db.Sections, "ID", "SectionName");
            ViewBag.FK_EmpRole = new SelectList(db.Roles, "ID", "RoleName");
            return View();
        }

        // POST: Employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,EmpId,EmpName,PwdKey,JoiningDate,ConfirmDate,LeToDays,IsAvail,IsActive,FK_Gender,FK_Designation,FK_Section,FK_EmpRole,UDT")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                employee.IsActive = true;
                employee.UDT = System.DateTime.Now;
                db.Employees.Add(employee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FK_Gender = new SelectList(db.Genders, "ID", "GenderType");
            ViewBag.FK_Designation = new SelectList(db.Designations, "ID", "DesignationM");
            ViewBag.FK_Section = new SelectList(db.Sections, "ID", "SectionName");
            ViewBag.FK_EmpRole = new SelectList(db.Roles, "ID", "RoleName");
            return View(employee);
        }

        // GET: Employees/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            ViewBag.FK_Gender = new SelectList(db.Genders, "ID", "GenderType");
            ViewBag.FK_Designation = new SelectList(db.Designations, "ID", "DesignationM");
            ViewBag.FK_Section = new SelectList(db.Sections, "ID", "SectionName");
            ViewBag.FK_EmpRole = new SelectList(db.Roles, "ID", "RoleName");
            return View(employee);
        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,EmpId,EmpName,PwdKey,JoiningDate,ConfirmDate,LeToDays,IsAvail,IsActive,FK_Gender,FK_Designation,FK_Section,FK_EmpRole,UDT")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                employee.UDT = System.DateTime.Now;
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FK_Gender = new SelectList(db.Genders, "ID", "GenderType");
            ViewBag.FK_Designation = new SelectList(db.Designations, "ID", "DesignationM");
            ViewBag.FK_Section = new SelectList(db.Sections, "ID", "SectionName");
            ViewBag.FK_EmpRole = new SelectList(db.Roles, "ID", "RoleName");
            return View(employee);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
