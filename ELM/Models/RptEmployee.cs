﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ELM.Models
{
    public class RptEmployee
    {
        public Nullable<int> Gender { get; set; }
        public Nullable<int> Designation { get; set; }
        public Nullable<int> Departments { get; set; }
        public Nullable<int> Sections { get; set; }
        public string  IsAvail { get; set; }
        public string IsActive { get; set; }
        public Nullable<int> LYear { get; set; }
        public Nullable<int> LType { get; set; }
    }
}