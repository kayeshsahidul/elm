﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ELM.Models
{
    public class MoreLeaveApplyVM
    {
        public Nullable<bool> CLFK_LeTitle { get; set; }
        public System.DateTime CLfrom { get; set; }
        public System.DateTime CLto { get; set; }
        public Nullable<bool> SLFK_LeTitle { get; set; }
        public System.DateTime SLfrom { get; set; }
        public System.DateTime SLto { get; set; }
        public Nullable<bool> ELFK_LeTitle { get; set; }
        public System.DateTime ELform { get; set; }
        public System.DateTime ELto { get; set; }
        public string LeaveReason { get; set; }
        public string WreStayInLeave { get; set; }
        public string LeDocuments { get; set; }
        public Nullable<bool> IsHalfLe { get; set; }
        public int FK_Employee { get; set; }
        public int LYear { get; set; }
        public decimal LeQty { get; set; }
        public Nullable<System.DateTime> UDT { get; set; }
    }
}