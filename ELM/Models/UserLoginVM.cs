﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ELM.Models
{
    //View model for user login and user Password change
    public class UserLoginVM
    {
        public string EmpId { get; set; }
        public string PwdKey { get; set; }
        public string footPrint { get; set; }

    }
}